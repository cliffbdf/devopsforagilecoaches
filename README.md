# DevOps For Agile Coaches

## Branches

* master
* bdd - includes Cucumber tests
* bdd-modules - reorganized into modules "main" and "features", with the Cucumber tests in "features".
* bdd-modules-docker - same as bdd-modules, but includes a Dockerfile,
* bdd-modules-notest - (unclear what this was for - perhaps delete)
* bdd-component - converted to a microservice
